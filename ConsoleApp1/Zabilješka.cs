﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class Zabilješka
    {
        private string autor;
        private string tekst;
        private string prioritet;

        public Zabilješka()
        {
            this.autor = "Nije ispunjeno!";
            this.tekst = "Nije ispunjeno!";
            this.prioritet = "0";
        }

        public Zabilješka(string Autor, string Tekst, string Prioritet) {
            this.autor = Autor;
            this.tekst = Tekst;
            this.prioritet = Prioritet;
        }

        public Zabilješka(string Tekst, string Prioritet)
        {
            this.tekst = Tekst;
            this.prioritet = Prioritet;
        }

        public Zabilješka(Zabilješka Objekt)
        {
            this.autor = Objekt.autor;
            this.tekst = Objekt.tekst;
            this.prioritet = Objekt.prioritet;
        }

        public void setPrioritet(string Prioritet) { this.prioritet = Prioritet; }
        public void setTekst(string Tekst) { this.tekst = Tekst; }

        public string getAutor()
        {
            return autor;
        }

        public string getTekst()
        {
            return tekst;
        }

        public string getPrioritet()
        {
            return prioritet;
        }

        public string Priority
        {
            get { return this.prioritet; }
            set { this.prioritet = value; }
        }

        public string Text
        {
            get { return this.tekst; }
            set { this.tekst = value; }
        }

        public string Name
        {
            get { return this.autor;}
        }

        public override string ToString()
        {
            return this.prioritet + "    " + this.tekst + ", " + this.autor;
        }
     
    }
}
