﻿using System;

namespace ConsoleApp1
{
    class Program
    {
       

        static void Main(string[] args)
        {
            Zabilješka zab1 = new Zabilješka();
            Console.WriteLine(zab1.getAutor());
            Console.WriteLine(zab1.getTekst());
            Console.WriteLine(zab1.getPrioritet());
            Console.WriteLine(zab1.ToString());

            
            Zabilješka zab2 = new Zabilješka("Stjepan", "TEEEEEEEEEEEEEKST", "2");
            Console.WriteLine(zab2.getAutor());
            Console.WriteLine(zab2.getTekst());
            Console.WriteLine(zab1.getPrioritet());
            Console.WriteLine(zab2.ToString());


            Zabilješka zab3 = new Zabilješka("ZAjaisdjoaisdjoj", "4");
            Console.WriteLine(zab3.getTekst());
            Console.WriteLine(zab3.getPrioritet());
            Console.WriteLine(zab3.ToString());


            VremenskaZabilješka vrime = new VremenskaZabilješka();
            Console.WriteLine(vrime.ToString());
            vrime = new VremenskaZabilješka("Bruno", "QWEqwe eqw", "4");
            Console.WriteLine(vrime.ToString());
        }
    }
}
